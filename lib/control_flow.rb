# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete(("a".."z").to_a.join)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str[(str.length - 1) / 2, str.length.odd? ? 1 : 2]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.count(VOWELS.join)
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce { |sum, num| sum *= num }
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ""
  arr.each do |el|
    result << separator if result.length > 0
    result << el
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ""
  (0...str.length).each do |i|
    result << (i.odd? ? str[i].upcase : str[i].downcase)
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = []
  str.split().each { |word| result << (word.length >= 5 ? word.reverse : word) }
  result.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  result = []
  (1..15).each do |i|
    if i % 15 == 0
      result << "fizzbuzz"
    elsif i % 5 == 0
      result << "buzz"
    elsif i % 3 == 0
      result << "fizz"
    else
      result << i
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  arr.each_index { |i| result[arr.length - i - 1] = arr[i] }
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (2..num/2).each {|i| return false if num % i == 0}
  num != 1
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  (1..num).each {|i| result << i if num % i == 0}
  result
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factors(num).select { |el| prime?(el) }
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  even, odd = nil, nil
  arr.each do |el|
    if el.odd?
      return even if odd && even
      odd = el
    else
      return odd if even && odd
      even = el
    end
  end
end
